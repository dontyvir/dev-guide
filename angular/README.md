# Angular Guidelines

En este apartado incluiremos algunos scripts que te ayudarán en la mantenibilidad del código de tus proyectos Angular. En principio, contamos con la primera versión de los siguientes scripts:

*  [**tslint.json**](tslint.json): En este file están incluidas las reglas de Codelyzer, Tslint y SonarTS propuestas para un proyecto Angular.

*  [**.prettierrc**](.prettierrc): Incluye las reglas para formatear el proyecto con Prettier.

*  [**.vscode/settings.json**](.vscode/settings.json): Incluye la configuración para formatear los scripts del proyecto al hacer Ctrol + S, en el IDE VSCode.

Para explicación detallada de cómo instalar estos guidelines en nuestro proyecto, revisar este [workshop en Confuence](https://bestado.atlassian.net/wiki/spaces/DTD/pages/308117564/Integraci+n+de+linters+en+Proyectos+Angular)

