# Guía de desarrollo para artefactos Node

En esta entrada, contemplamos reglas de estilo en 3 dimensiones: formato, mejores prácticas, uso de librerías oficiales.

## Formato

### - Nombre de variables en inglés, nombres propios en español
Ejemplo:

account   -> inglés

cuentaRUT -> español

### - Nombre de funciones en camelCase
Ejemplo: getTotalAccounts

### - Las constantes inmutables deben ir en UPPERCASE
Todas las estructuras que no tengan cambios en el tiempo, deben ir en letras mayúsculas.

### - Nombre de files en lowercase
Linux es case-sensitive, es decir diferenciará un nombre que esté en mayúscula de uno que esté en minúscula.
Ejemplo: 
let MyClass = require('my-class');


### - Número de líneas máximas por file 300
Para cada script, las líneas totales no deberían superar las 300 líneas.


### - Documentar código en español
La documentación del código, se realizará en español para instar a declarar detalles que sean necesarios.

### - Nombres de objetos en Pascalcase
Los objetos y nombres de clases deben ir con la primera letra en mayúscula.

### - Nombres de directorios de proyecto en snakecase (Opcional)
Los nombres de los directorios deberían ir con la siguiente nomenclatura: nombre_largo_directorio.

### - Nombres de files con dashed case (Opcional)
Los nombres de archivos de cada script deben ir con guiones. Ejemplo: casa-grande.js.

### Evitar colocar endpoints en duro en el artefacto, siempre hacer referencia al service:
En las fuentes no debe versionarse los endpoints en duro de servicios terceros. Siempre debe configurarse en los endpoints del values.yaml:

```
//Caso erroneo
obtenerDatosPersonales: ENDPOINT_MICROSERVICIOS_URL + "/servicios/datosclienteemt/microservicios/ocvt/v2/obtenerDatosPersonales"
```
ó
```
var config = {
	produccion: {
		endpointsServicios: {
			verifyCustomerIdentification: 'http://' + process.env.ENDPOINT_BUS_PRODUCCION + ':' + process.env.VERIFYCUSTOMERIDENTIFICATION_PUERTO_PRODUCCION,
			
		},
....

//Caso exitoso
values.yaml
URL_GENERA_TOKEN: http://generatoken-v1-application-service.compartidoms.svc:3000/microservicio/v1/generaToken/generaToken

```


### - Actualizar README
Por cada endpoint que se agregue, indicar inputs/outputs y mantenedor de tal operación. Además, si es un microservicio, incluirlo en el siguiente listado: [Listado de Microservicios](https://bestado.atlassian.net/wiki/spaces/ADM/pages/103612451/Lista+Microservicios)


### - Uso de linters 
Actualmente, se recomienda el uso de Eslint + prettier + airbnb, de forma que cada desarrollador haga revisión estática de su código en forma local. Las reglas que se usan son las siguientes:

[es-lint-config-airbnb-base](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb-base/rules)
 

1. instalar extensiones de visual estudio code [eslint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) y [prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
2. instalar librerias necesarias en tu proyecto: npm install --save-dev
    * eslint
    * eslint-config-airbnb-base
    * eslint-plugin-import
    * eslint-config-prettier
    * eslint-plugin-prettier
    * prettier
    
>npm install save-dev eslint eslint-config-airbnb-base eslint-plugin-import eslint-config-prettier eslint-plugin-prettier prettier

3. crear archivos de configuración .eslintrc y .prettierrc
4. habiliar "editor.formatOnSave" para hacer el formateo del código.
5. habilitar prettier-eslint instancia de prettier. es la configuración de la extensión de prettier en vs code.

## Mejores Prácticas

### Usar express como framework
Esto aplica tanto para piezas BFF como Microservicios. Ls piezas que ya existen en micro, deben permanecer así.

### Los microservicios deben ser servicios stateless
Evitar en lo posible incluir mucha lógica que contemple guardado de estados o procesamiento de información que proviene del servicio BUS.

### Estándarizar verbos en endpoints
El enfoque a utilizar debe ser Restful, hacer uso de los verbos http y apuntar a definir bien el dominio.
Esto en pro de evitar que se vayan generando endpoints particulares para cada célula. Verificar endpoints redundantes antes de agregar alguna operación nueva


### - Guardar las mismas versiones que el repo con save-exact
Sólo los dueños de cada artefacto deberían cambiar las versiones. Mantener la instalación de paquetes con el símbolo(~).

### - Evitar colocar información sensible en el código
Guardar accesos, urls, credenciales en variables de entorno.

### - Refactorizar código redundante
En un futuro se restringirá en el pipeline mediante Sonar, el código duplicado.

### - Cada artefacto debe tener healthcheck
Hay 4 tipos de healthcheck detectados:
- Simple
- Mongo
- Redis(Incluir siempre y cuando haya dependencia en la lógica de la función)
- Mysql
Revisar el [template](util/healthcheck.js) disponible.

### - Evitar uso de var
En su lugar usar let y const.

### - Evitar bloques de código comentado
Se sugieren dos formas de evitarlo:
- Colocar el extracto de código en una función que no se llame.
- Guardar el código en una rama.

### - Evitar uso de eventos
Esto en pro de evitar memory leaks. Más información en el chapter de desarrollo de [Aprendizajes de Swat](https://bestado.atlassian.net/wiki/download/attachments/263127424/%5BChapter%5D24_09_AprendizajesSwat.pdf?api=v2)


### - Evitar condicionales muy anidados.
Hacer uso de return early, visto en el chapter [Mejoras Condicionales](https://bestado.atlassian.net/wiki/download/attachments/263127424/%5BChapter%5D20-08_MejorasCondicionales.pptx?api=v2)


### - Normalización headers en Servicios BUS.
Esto ayudará a depurar errores bus mediante la herramienta Techlog de forma más expedita. Para realizar el cambio, tomar de referencia las siguientes entradas:
- [Chapter Headers Bus](https://bestado.atlassian.net/wiki/download/attachments/263127424/%5BChapter%5D08-04_Headers%20Bus%20y%20ES6.pptx?api=v2)


### - Uso de Json Schemas/ pruebas de contrato u otro (Opcional)
Hacer uso de validadores de los datos del request con el fin de evitar errores por falta de datos.

### - Complejidad ciclomática de rango 10 por función (Opcional)
Limitar el número de flujos por función a no más de 10.


### - Evitar usos de .then, en vez usar async/await (Opcional)
Además de esto, controlar errores con try/catch.

## Librerías oficiales
En el siguiente [enlace](https://docs.google.com/spreadsheets/d/1c2oGfuRb_B_7UF_Z6cn21HacziiUlAfQz2xkrG2Njk8/edit?usp=sharing) se pueden conseguir las librerías existentes en nexus. 
### - No usar console.log
En su lugar, hacer uso de la librería logger-bech en la versión 2.0.0. Para acceder al ejemplo, entra [aquí](util/libs_examples/logger-bech.md).

### - Librería de redis
Actualizar a la versión redis-bech 2.0.4. El ejemplo se encuentra [aquí](util/libs_examples/redis-bech.md).

### - [WIP] Librería monitoreo
monitoreo-bech@2.0.3. Esta versión realiza un cambio de estructura en lo que se enviará a monitoreo. Puedes ver el ejemplo [aquí](util/libs_examples/monitoreo-bech.md).
Se está trabajando en centralizar toda la configuración concerniente a monitoreo en el microservicio monitoreo-ms.

### - Migrar a strongsoap
Sólo artefactos que consuman servicios BUS de broker 9 en adelante. Librería disponible soap-client-bech: 3.0.2


### - Validación de token:
Hay 3 tipos de token que se están usando:
- Token simple.
- Token persona natural
- Token persona jurídica (compuesto).
Librería oficial para manejo de token:  jwt-bech 0.9.3. 

### - Envío a log digital
Para journalizar las transacciones de nuestros aplicativos, disponemos de la librerías journal-bech en su versión 2.1.1.

### - Generador de artefactos back
Al momento de crear un nuevo BFF o Microservicio, valerse de la siguiente herramienta:
[generador-bech](https://gitlab.banco.bestado.cl/Framework/generador-bech)








