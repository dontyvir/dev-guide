// Redis-bech
const db = require('../util/sequelize');
const { cache } = require('redis-bech');

// mongoose
const ClientData = require("../model/clientData.model");
const mongoose = require('mongoose');

// bech-mongo-client
import MongoModule from "bech-mongo-client";

// mysql knex
const mysql = require('../../config/knexConfig').knexConnection;

// mysql sequelize
const db = require('../util/sequelize');

// Simple
//Micro
const { send } = require('micro');
//express


module.exports = {
    healthcheck: async (req, res) => {
        let status, body;
        // Redis
        try {
            if (cache.isConnected()) {
                status = 200;
                body = {'redis': 'OK'};
            } else {
                status = 500;
                body = {'redis': 'NOOK'};
            }
        } catch (error) {
            status = 500;
            body.redis = 'NOOK'
        }
        // Mongo con mongoose
        try {
            if (mongoose.connection.readyState == 1) {
                const QUERY_DATOS = "BFF_AHO_11111111";
                let resp = await ClientData.get(QUERY_DATOS);
                if (resp) {
                    status = 200;
                    body = {'bd': 'OK'};
                } else {
                    status = 500;
                    body = {'bd': 'NOOK'};
                }
            } else {
                status = 500;
                body = {'bd': 'NOOK'};
            }
        } catch (error) {
            status = 500;
            body = {'bd': 'NOOK'};
        }

        // Mongo con bech-mongo-client
        try {
            let mongoModule = new MongoModule();
            let resp = await mongoModule.findByQuery('',{},{});
            if (Array.isArray(resp)) {
                status = 200;
                body = {'bd': 'OK'};
            } else {
                status = 500;
                body = {'bd': 'NOOK'};
            }
          } catch (error) {
            status = 500;
            body = {'bd': 'NOOK'};
          }

        // Mysql con knex
        try {
            await mysql.raw("SELECT 1");
            status = 200;
            body = {'bd': 'OK'};
        }catch (error) {
            status = 500;
            body = {'bd': 'NOOK'};
        }

        // Mysql sequelize
        try {
            await db.sequelize.authenticate();
            status = 200;
            body = {db: 'OK'};
            body = { db: 'OK' };
        } catch (error) {
            status = 500;
            body = {db: 'NOOK'};
            body = { db: 'NOOK' };

        }

        // Simple
        // micro
        send(res, 200, "OK");

        // express
        res.status(200).send({ "estado": 200, "mensaje": "OK" })



        //---------------------------
        res.status(status).send(body);
    }
}