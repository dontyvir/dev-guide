# Checklist para actualizar librería logger-bech

## 1.- Instalación

```
>$ npm install --save logger-bech@latest
```
## 2.- Uso Simple

Para loguear en files como server.js o routes, en dónde aún no tenemos contexto del request, podríamos usar la implementación simple:


```javascript
//Importamos
const loggerBECH = require('logger-bech').loggerBECH;
```

```javascript
//Usamos según el nivel que queremos loguear. Recordar que la librería, antepone el nombre del script de dónde proviene el log. Por tanto, sólo basta indicar el método en dónde se genera el log o alguna marca en particular:
loggerBECH.info(`[serverStart]: Iniciando microservicio ValidaOTP en puerto 3000 ${optionsExpress.port}`);
```

Lucirá así:
```
[2019-12-11T14:24:36.728 INFO server.js][serverStart]:Iniciando microservicio ValidaOTP en puerto 3000
```

## 3.- Uso con trackID y codigosession

Por el contrario, si queremos realizar la traza de un request completo, tendremos que loguear el código de sesión y el xtrackId, desde que se gatilla tal request. Para ello nos valdremos de un módulo utilitario que instance tales argumentos una sola vez y se propaguen hacia los módulos internos.

```javascript
//modulo config.js
const loggerBECH = require("logger-bech");
let logger;

function startLogger(xtrackid, sessionCode) {
    logger = loggerBECH.getLoggerBECH(xtrackid,sessionCode);
}

function getLogger() {
    return logger;
}

module.exports = { startLogger, getLogger };
```

Luego que tenemos expuesto el logger como un singleton, procedemos a ocuparlo en nuestros respectivos controladores, siguiendo el modelo de 3 capas explicado previamente:
Controller -> Módulo -> Service

### Ocupandolo en el Controller

En este script, recibirimos la petición y podremos instanciar los datos que necesitamos, que son codigosession y xTrackID.

```javascript
//controller A
let { startLogger, getLogger } = require("../../config/config");

async function initProcess(req, res) {
  startLogger(req.headers.xtrackid, req.headers.codigosesion);
  const logger = getLogger();
  logger.trace(`[initProcess]: ---- Empezando request-----`);
  ....

}
```

### Ocupandolo en el Módulo y en el Service

Aquí basta con recuperar el objeto logger ya instanciado en el controlador A, a través de la función getLogger():

```javascript
//Módulo A
const { getLogger } = require("../../config/config");
async function getDrupalContent(autenticationData) {
  try {
    const drupalContent = await DrupalModule.getContent();
    getLogger().debug(`[getDrupalContent]: drupal content: ${drupalContent}`);

    ....
  
```
En el caso del service, se puede implementar de la misma forma.

## 4.- Levels y situaciones

Para revisar las situaciones en las que se puede ocupar, revisar láminas del [chapter del 26/11](https://bestado.atlassian.net/wiki/download/attachments/263127424/%5BCHAPTER%5DRefactorYCodeReview_26_11.pdf?api=v2)


## 5.- Configuración level

Se puede configurar el nivel de logs que deseen que tengan sus salidas:

TRACE ( menos restrictivo)

DEBUG

INFO

WARN

ERROR

FATAL

OFF   (más restrictivo)

Estos valores se pueden configurar a través de la variable de entorno LOGGERLEVEL, si no se instancia por defecto empieza a logguear desde INFO hasta OFF(este nivel apaga inmediatamente los logs para ese servicio en particular)

## 6.- Revisar pruebas unitarias

Al momento de generar el stub para mokear la librería, se puede usar la siguiente estructura:

```javascript
const depStub = {
  "../../config/config": {
    getLogger: function() {
      let logger = {};
      logger.error = function() {
        return "";
      };
      logger.trace = function() {
        return "";
      };
      logger.debug = function() {
        return "";
      };
      return logger;
    },
    startLogger: function() {}
  }
  ...
}

```

