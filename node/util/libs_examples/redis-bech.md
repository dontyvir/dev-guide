# Checklist para actualizar librería monitoreo-bech

## 1.- Instalación
Versión actual: "redis-bech": "^2.0.4",
```
>$ npm install --save redis-bech@latest
```
## 2.- Configuración

Para ejecutarlo en local, es necesario agregar estas variables de entorno en su sistema:
```
export HOST_REDIS=172.17.200.7
export PORT_REDIS=6379
export PASS_REDIS=contrasenaredis_2017
export EXPIRACION_REDIS=300
```

Para desplegarlo en Kubernetes, deben referenciarlo a través del service:
```
HOST_REDIS: ibm-redis-ha-dev-int-v2-ibm-redis-ha-dev-master-svc //(si es un microservicio, colocar ibm-redis-ha-dev-v2-ibm-redis-ha-dev-master-svc) 
PORT_REDIS: 6379
SECRET_REDIS: ULTRASECRETO
PASS_REDIS: ''
EXPIRACION_REDIS: 300
```

Para esta versión de la librería, necesitaremos las siguientes variables de entorno:
```javascript
// config.js

/**
 * función de validación de variables de entorno
 * @param {type} paramName
 * @param {type} required
 * @param {type} config
 * @returns {addEnvParam.parameter|process.env}
 */
function addEnvParam(paramName, required, config) {

   // evaluación del parámetro
   var parameter = process.env[paramName];
   
   if (!parameter){
      
      if(required)
         throw new Error("No se definió la variable de entorno " + paramName);
      else
         return null;
   }
   
   if (config)
      config[paramName] = parameter;

   return parameter;
}

const redis = {
    host:addEnvParam("HOST_REDIS", true),
    port:addEnvParam("PORT_REDIS", true),
    expiration: addEnvParam("TOKEN_EXPIRACION", false) || 300,
    cryptSecret: addEnvParam("SECRET_REDIS", false),
    pass: addEnvParam("PASS_REDIS", false)
};
module.exports = { redis };

```
Luego, en el script en el que levanten el server, inicializar la instancia de redis:


```javascript
const cache = require('redis-bech').cache;
const redis = require('./config/config').redis;

// Inicializar la instancia de redis con la configuración obtenida de las variables de entorno:
async function conectarCache(){
    await cache.conectar(redis.host,redis.port, redis.pass);
    cache.setConfig(redis.expiration,redis.cryptSecret);

}
// Cuando se quiera finalizar el proceso, desconectar de redis explícitamente:
async function cleanup(){
    loggerBECH.info(`[index.js][cleanup]: Desconectando...`);
    await cache.desconectar();
    process.exit(0);
}

// Conectamos
conectarCache();

process.on('SIGTERM', cleanup);
process.on('SIGINT', cleanup);
process.on('SIGHUP', cleanup);



```

## 3.- Comparativa métodos expuestos con otras librerías:

Para que se haga más rápida la migración, disponemos de esta [tabla comparativa](https://docs.google.com/spreadsheets/d/1GR3BYlIugBSWP-OXOpogPRrKYENnyzcCBlHrPuGpxT8/edit?usp=sharing) que nos permitirá decidir cuál método usar:

![Tabla](tabla_comparativa.png "Comparación")

```javascript
//Ejemplo guardar en caché
cache.setEncrypted(
    codigosesion, // o Id de redis
    JSON.stringify(autenticationData), // data a guardar
    redis.expiration  // valor seteado en environments
);

// Ejemplo borrar en caché:
cache.getConnection().del(
    codigosesion, // o Id de redis 
    function (err, value) { // callback
            if (value == 1) {
                // Se logró borrar el objeto con codigosesion
            } else {
                // No se logró borrar el objeto con codigosesion
            }
        });
    }
```

## 4.- Revisar pruebas unitarias

Al momento de generar el stub para mokear la librería, se puede usar la siguiente estructura:

```javascript
const depStub = {
  'redis-bech': {
        cache:{
          conectar: function () {

          },
          getEncrypted: function (key) {
            return Promise.resolve({});
          },
          desconectar: function () {

          },
          getConnection: function () {
            return {
              del: function (key, callback) {
                callback("", 1);
                return "OK";
                done();
              }
            }
          }
        }
      },
  ...
}

```

## 5.- Deployando en ambientes

Para validar que el artefacto cumpla con todo lo necesario, revisar el checklist que se planteó en el [chapter del 16/12](https://bestado.atlassian.net/wiki/download/attachments/263127424/%5BCHAPTER%5DRevisi%C3%B3n%20procesos%20Paso%20ambientes-16_12.pdf?api=v2).

