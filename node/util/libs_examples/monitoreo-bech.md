# Checklist para actualizar librería monitoreo-bech

## 1.- Instalación
Versión actual: "monitoreo-bech": "^2.0.4"
```
>$ npm install --save monitoreo-bech@latest
```
## 2.- Configuración

Para esta versión de la librería, necesitaremos tener configurados los siguientes valores en el file values.yaml de nuestro artefacto:

```javascript
globalSecrets:
  KAFKA_USERNAME: global-kafka-secret
globalConfig:
  KAFKA_BROKERS: global-kafka-config
  KAFKA_DOMAIN: global-kafka-config
  KAFKA_KEYTAB: global-kafka-config
```
Esta variable global-kafka-config, internamente tiene un json con los valores necesarios para que nuestro artefacto se conecte a kafka:

![config](kafka_config.png "Kafka Config")


## 3.- Nuevos atributos a instanciar:
La nueva librería hace uso de los [siguientes parámetros](https://docs.google.com/spreadsheets/d/1s0ySytsyPV4P0fGgtL2VRDkHsMlfvYA7srqL-fuNSmE/edit?usp=sharing), unos provenientes del header y otros que dependen del controller en el que se esté recibiendo el request:

![estructura](estructura_monitoreo.png "Estructura Monitoreo")

Esto servirá para poblar la estructura final que toma en consideración la librería:
```javascript
const returnData = {
// Campos de monitoreo
    id: _id,
    canal: headers.canal,
    funcionalidad: headers.funcionalidad,
    etapa: headers.etapa,
    fechaInicio: fechaMonitoreo(headers.fechaInicio), // Transforma a hora local 2019-12-23T14:55:30.981Z
    fechaTermino: fechaMonitoreo(headers.fechaTermino),
    rutPersonaNatural: headers.rutPersonaNatural,
    rutPersonaEmpresa: headers.rutPersonaEmpresa,
    nombreAplicacion: headers.nombreaplicacion,
    codigoAplicacion: headers.codigoAplicacion,
    tipoArtefacto: headers.artefacto,
    xTrackID: headers.xtrackid,
    // Campos para debug
    nombreArtefacto: headers.nombreArtefacto,
    url: _url,
    operacion: headers.operacion,
    codigosesion: headers.codigosesion,
    ipCliente:
    headers["X-Forwarded-For"] ||
    headers["x-forwarded-for"] ||
    connection.remoteAddress,
    dispositivo: Buffer.from(headers["user-agent"] || "undefined").toString(
    "base64"
    ),
    codigoRespuesta: res.statusCode,
    datosPeticion: requestPayload,
    datosRespuesta: responsePayload
    };
```
## 3.- Sugerencia de implementación

Esta es una de las formas en las que se implementó la librería para monitorear las salidas de un bff. La solución radica en delegar tanto la construcción de la estructura de monitoreo como el envío del mismo en un módulo especializado.



```javascript
//Módulo Monitor
const { monitoreoBECH } = require("monitoreo-bech");
const { getLogger } = require("../../config/config");
const uuidv1 = require('uuid/v1');

/**
 * Funcion que completa los datos del header para el monitoreo
 * @param {JSON} req 
 * @param {JSON} params 
 */
function prepareInputs(req, params) {
  let headers = req.headers;
  headers.codeFuncionalidad = headers.funcionalidad
    ? headers.funcionalidad
    : "ROL@AU001"; // Cambiar según artefacto
  headers.funcionalidad = "Cambio de clave"; // Cambiar según artefacto
  headers.fechaInicio = new Date();
  headers.nombreArtefacto = "BFF Autenticación"; // Cambiar según artefacto
  headers.artefacto = "BFF";
  headers.etapa = params.stage;
  headers.rutPersonaNatural = params.rut;
  headers.operacion = params.operation;
  headers.xrequestid = uuidv1();
}

/**
 * Funcion encargada de realizar una petición de registro para el monitoreo
 * @param {JSON} req 
 * @param {JSON} res 
 * @param {JSON} requestPayload 
 * @param {JSON} responsePayload 
 * @param {String} codeMonitoring 
 */
async function recordMonitoring(
  req,
  res,
  requestPayload,
  responsePayload,
  codeMonitoring
) {
  
  req.headers.codigoAplicacion = codeMonitoring;
  req.headers.fechaTermino = new Date();
  getLogger().info(`Entrando a monitorear ${JSON.stringify(req.headers)}`)
  try {
    let response = await monitoreoBECH(
      req,
      res,
      requestPayload,
      responsePayload
    );
    getLogger().trace(`Resultado registro en monitoreo ${response}`)
  } catch (error) {
    getLogger().error(`Error al realizar monitoreo: ${error}`);
  }
}

module.exports = { prepareInputs, recordMonitoring };

```

Luego, al momento de ocuparlo en el controler, hacemos lo siguiente:

```javascript
// Al inicializar el controller, recuperar los valores que necesitamos:
const paramsMonitoring = {
    stage: "Grabar clave",
    rut: req.headers.rut + "-" + req.headers.dv,
    operation: "Cambio de clave",
    startDate: new Date()
  };
MonitoringModule.prepareInputs(req, paramsMonitoring);


// Justo al momento de monitorear, invocarlo con los siguientes atributos:
MonitoringModule.recordMonitoring(
      req,  
      res,
      { panId: req.body.panId, clave: "XXXX" },  // requestPayload
      `[validateInputsRequest]: error:BAU.CCL.0001: 'Uno o más parámetros no fueron definidos' ${errorInput}`, // responsePayload
      "BAU.CCL.0001"
);

```

## 5.- Validar logs en Hive
Finalmente, para validar que los logs están saliendo bien desde nuestro artefacto, revisar la vista bitacora_logs3.

![bitácora](hive.png "Hive")

## 6.- Revisar pruebas unitarias

Al momento de generar el stub para mokear la librería, se puede usar la siguiente estructura:

```javascript
const depStub = {
  "./monitoring.module": {
    prepareInputs: function() {},
    recordMonitoring: function() {}
  },
  },
  ...
}

```