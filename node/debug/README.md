# Debug de servicios node

_Richard Pinochet Morales, marzo 2018_

## Ejecutar en modo debug remoto
Validar que el microservicio puede resolver todas sus dependencias en el perfil “Default” al momento de ejecutarlo.
Luego ejecutar con modo debug. Por defecto node asigna el puerto 9229 para depurar.

```shell
node --inspect server.js
```

## Script de carga
Existen varias herramientas para cargar el servicio, lo importante es tener en cuenta un par de conceptos para que las pruebas representen un escenario similar a uno real:
* __Usuarios concurrentes__: cantidad de hilos “paralelos” que levantará la herramienta de testing, que permitirá dar carga a nuestro servicio
* __Timeouts de servicio__: tiempos de respuesta máximos que permitiremos en la prueba, en caso de colapsar la máquina o de encontrar un leak. Es bueno tener un fallo en la prueba lo antes posible, para dedicar más tiempo a diagnosticar el problema.
* __Cantidad de request__: En algunas herramientas de testeo podemos configurar una cantidad de request como marco para la prueba, tener en consideración que esta cantidad debe ser mayor que la cantidad de usuarios concurrentes.
* __Tiempo de prueba__: Es el tiempo límite para la prueba, el fin de esto, es intentar conseguir la mayor cantidad de request en el tiempo propuesto.

Una herramienta muy útil y sencilla de utilizar es apache AB, es un programa que puede ser ejecutado en linux a través de la línea de comandos, dando flexibilidad y simpleza al ambiente de desarrollo.

<https://httpd.apache.org/docs/2.4/programs/ab.html>

Los argumentos destacables de esta herramienta son:

```shell
-c [usuario concurrentes]
-n [cantidad de request]
-H [headers:valor para el servicio]
-s [timeout para el servicio en segundos]
```

__Comando de ejemplo__:

```shell
ab -n500 -c10 -H "Authorization:qwertyqweertyqwerty" -H "codigosesion:test" -H "canal:MB" -H "nombreaplicacion:AHORRO" -s "30" http://localhost:3005/servicios/datosCliente/microservicios/ocvt/v1.0.0/obtenerDatosCliente/identificationNumber/12345678/identificationComplement/9
```

## Herramientas para la depuración
### Herramienta de google-chrome para depurar
Desde node 6.x.x puede depurarse de manera remota el programa escrito en node, para esto google chrome o visual studio code, entre otros nos ofrecen una herramienta de depuración especializada. [Aquí mas información](https://medium.com/@paul_irish/debugging-node-js-nightlies-with-chrome-devtools-7c4a1b95ae27)

Con esta herramienta podemos analizar el código cargado en memoria en el servidor, pudiendo realizar cambios en caliente, agregar breakpoints, hacer profiling, ejecutar scripts directo en consola, entre otros.

Primero deben configurarse los “Remote Target”, un ejemplo puede ser el servicio en el host “localhost” con puerto “9229”
En caso de estar ejecutándose el servicio, este se cargará en la herramienta y podrá ser analizado en con el devtool especializado de node.

![alt text][logo]

[logo]: https://gitlab.banco.bestado.cl/DevGroup/dev-guide/raw/ec9f46a782db8c4f06f3008e5422c9e0f74d74d0/node/debug/img/Captura%20001.PNG ""

Podemos posteriormente revisar el código y agregar breakpoints en los lugares críticos para el diagnóstico:

![alt text][logo2]

[logo2]: https://gitlab.banco.bestado.cl/DevGroup/dev-guide/raw/29fa684170b2ff242d9766402dba106212f430c3/node/debug/img/Captura%20002.PNG ""

Es importante destacar, que al momento de llegar a un breakpoint, podemos realizar el tracking de la operación a través del call stack o simplemente poner el cursor del mouse sobre las sentencias previas al breakpoint para saber cuáles son sus valores en tiempo de ejecución. En la siguiente figura un ejemplo:

![alt text][logo3]

[logo3]: https://gitlab.banco.bestado.cl/DevGroup/dev-guide/raw/29fa684170b2ff242d9766402dba106212f430c3/node/debug/img/Captura%20003.PNG ""

Adicional a esto, podemos hacer profiling de memoria del servicio. Algo muy útil cuando sospechamos tener leaks de memoria.

![alt text][logo4]

[logo4]: https://gitlab.banco.bestado.cl/DevGroup/dev-guide/raw/29fa684170b2ff242d9766402dba106212f430c3/node/debug/img/Captura%20004.PNG ""

Cuando buscamos leak de cpu o sobrecarga en tiempos de respuesta, es recomendable hacer profile de cpu:

![alt text][logo5]

[logo5]: https://gitlab.banco.bestado.cl/DevGroup/dev-guide/raw/29fa684170b2ff242d9766402dba106212f430c3/node/debug/img/Captura%20005.PNG ""

En este ejemplo podemos ver que hay una sentencia que consume el 47% del tiempo total. Para esclarecer esto, podemos cambiar la vista de tree (Top Down) a Bottom Up y descubrir cuál fue la cadena de sentencias que aumentan nuestros tiempos de respuesta:

![alt text][logo6]

[logo6]: https://gitlab.banco.bestado.cl/DevGroup/dev-guide/raw/29fa684170b2ff242d9766402dba106212f430c3/node/debug/img/Captura%20006.PNG ""

Y de manera muy rápida podemos apreciar que las llamadas a bus, utilizan una librería para interpretar xmls (XMLHttpRequest) que hace bloqueo en las transacciones, aumentando los tiempos de respuesta.

## Elementos importantes para diagnosticar.

Al momento de hacer estos ejercicios, que nos permiten tener un producto más estable de cara a cliente y de cara a nuestra infraestructura, se debe hacer un recuadro resumen con los datos relevantes de la prueba y adjuntar también la información completa, en caso de quedar alguna duda. Puede seguir la siguiente sugerencia para hacer un informe de pruebas de carga manual:

### Evidencias:
Pruebas de carga locales, apuntando a bus de servicios de QA.
#### Prueba 1

Cantidad de request a resolver:200

Usuarios Concurrentes: 5

Timeout configurado: 30 segundos.

Resultados:

| Resumen: | resultados |
|-|:-| 
| Request Completados | 200 | 
| Request fallidos | 0 | 
| TPS| 1.28 | 
| Tiempo mínimo de respuesta | 773 | 
| Tiempo máximo de respuesta | 7211 | 
| Promedio | 3889 | 
| Desviación estándar | +/- 900.6 | 

| Resumen | resultados |
|-|:-| 
| Document Path |         /servicios/datosCliente/microservicios/ocvt/v1.0.0/obtenerDatosCliente/identificationNumber/13278669/identificationComplement/0
| Document Length |        409 bytes
| Concurrency Level |      5
| Time taken for tests |   156.502 seconds
| Complete requests |      200
| Failed requests |        0
| Total transferred |      190776 bytes
| HTML transferred |       81800 bytes
| Requests per second |    1.28 [#/sec] (mean)
| Time per request |       3912.550 [ms] (mean)
| Time per request |       782.510 [ms] (mean, across all concurrent requests)
| Transfer rate |          1.19 [Kbytes/sec] received

| Connection Times (ms) | | | | | |
|-            |-      |-            |-        |-      |-      |
|             |  min  | mean[+/-sd] | median  |  max  | cant  |
| Connect     | 0     | 0           | 0.0     | 0     | 0     |
| Processing  | 773   | 3889        | 900.6   | 3878  | 7211  |
| Waiting     | 772   | 3887        | 900.6   | 3877  | 7210  |
| Total       | 773   | 3889        | 900.6   | 3878  | 7211  |

Percentage of the requests served within a certain time (ms)

| percentil | numero RQ |
|:-:    |-      |
| 50%   | 3878  |
| 66%   | 3957  |
| 75%   | 4003  |
| 80%   | 4034  |
| 90%   | 4704  |
| 95%   | 6222  |
| 98%   | 7073  |
| 99%   | 7095  |
| 100%  | 7211  (longest request)| 


### Prueba 2
Cantidad de request a resolver:500

Usuarios Concurrentes: 10

Timeout configurado: 30 segundos.

Resultados:


| Resumen | Resultados |
|--------------------|:-|
| Request Completados | 500|
| Request fallidos | 0 |
| TPS | 1.18 |
| Tiempo mínimo de respuesta | 1629 |
| Tiempo máximo de respuesta | 16074 |
| Promedio | 8437 |
| Desviación estándar | +/- 1091.8 |

| Resumen | Resultados |
|--------------------|:-|
| Document Path | /servicios/datosCliente/microservicios/ocvt/v1.0.0/obtenerDatosCliente/identificationNumber/13278669/identificationComplement/0 |
| Document Length | 409 bytes |
| Concurrency Level |      10  |
| Time taken for tests |   423.870 seconds |
| Complete requests |      500 |
| Failed requests |        0 |
| Total transferred |      476998 bytes |
| HTML transferred |       204500 bytes |
| Requests per second |    1.18 [#/sec] (mean) |
| Time per request |       8477.390 [ms] (mean) |
| Time per request |      847.739 [ms] (mean, across all concurrent requests) |
| Transfer rate |          1.10 [Kbytes/sec] received |

| Connection Times (ms) | | | | | |
|-            |-      |-            |-        |-      |-       |
|             | min   | mean[+/-sd] | median  | max   | cant   |
|Connect      |    0  |    0        |   0.0   |    0  |     0  |
|Processing   | 1629  | 8437        | 1091.8  |  8480 |  16074 |
|Waiting      | 1628  | 8433        | 1091.4  |  8475 |  16048 |
|Total        | 1629  | 8437        | 1091.8  |  8481 |  16074 |

Percentage of the requests served within a certain time (ms)

| percentil | numero RQ |
|:-:    |-      |
|  50%  |  8481 | 
|  66%  |  8547 | 
|  75%  |  8565 | 
|  80%  |  8577 | 
|  90%  |  8674 | 
|  95%  |  8995 | 
|  98%  |  9555 | 
|  99%  | 12260 | 
| 100%  | 16074 (longest request) |

## Conferencia Netflix de depuración de servicios NODE
En esta conferencia se muestran herramientas puntuales para hacer debug y profiling de servicios node. Además Indican algunas buenas prácticas para el diagnóstico de errores en producción.
https://youtu.be/O1YP8QP9gLA

![alt text][logo7]

[logo7]: https://gitlab.banco.bestado.cl/DevGroup/dev-guide/raw/29fa684170b2ff242d9766402dba106212f430c3/node/debug/img/Captura%20007.PNG

