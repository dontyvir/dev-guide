# Guía de desarrollo BECH

Esta es nuestra guía de desarrollo. 
>De nosotros para nosotros.

En este repositorio incluiremos las versiones de las guías de estilo que se vayan acordando como equipo. Todos estamos invitados a aportar en la construcción de los mismos.


## Estructura

* [Guidelines Proyectos Angular](angular/README.md)
* [Guidelines Proyectos Node](node/README.md)
    * [Apoyo debug y profiling servicios node](node/debug/README.md)


