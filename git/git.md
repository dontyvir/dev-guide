# GIT


Para poder ordenar nuestros proyectos es necesario sugerimos la siguiente estrategia de branching para llevar el control de versión de nuestros proyectos.

## Estrategia de Ramificación
---


Los repositorios de los proyectos tienen 2 ramas base por defecto:

- master
- Desarrollo-CI


A medida que avanza el proyecto, se crean ramas a partir de `Desarrollo-CI` asociadas al entregable en el que se trabaja, estas ramas son denominadas **Release**.


<img src="https://gitlab.banco.bestado.cl/DevGroup/dev-guide/raw/master/img/r1.png" alt="r1" />



A su vez de cada **Release** se generan ramas por **Historias de Usuario** o **Tareas Tecnicas**.


<img src="https://gitlab.banco.bestado.cl/DevGroup/dev-guide/raw/master/img/ramas.png" alt="ramas" />



En resumen tenemos una estructura de ramas de 4 niveles:

```java

master                 // 1° nivel - rama de producción
    └─ Desarrollo-CI     // 2° nivel - rama de integración continua
        └─ Release       // 3° nivel - rama de entregables
            └─ Historias/Tareas Técnicas   // 4° nivel - rama de desarrollo

```

### Nombramiento de Ramas

El nombre de las ramas se define de acuerdo a su nivel:

- Nivel 1: este nivel solo tiene una rama **master**
- Nivel 2: este nivel solo tiene una rama **Desarrollo-CI**
- Nivel 3: este nivel contiene **n** ramas y se nombran segun el siguiente formato:

    ```
    release[numero de entregable]
    ```

    **Ejemplo:**
    ```
    release1
    ```

**Excepción**

Para el caso del primer entre esta rama se nombrara **mvp**

- Nivel 4: esta rama contiene **n** ramas y se nombran según el siguiente formato:

    ```
     R[numero de entregable]/[codigo jira]_[descripcion corta]
    ```
    **Ejemplo:**

    <img src="https://gitlab.banco.bestado.cl/DevGroup/dev-guide/raw/master/img/cod-jira.png" alt="cod-jira" />

    ``` 
     R3/PCDA-415_vivienda
    ```

### Rama Master

### Rama CI

## Estrategia de Commits

Los mensajes de commit:

  * siempre los hacemos en español
  * normalmente tienen una sola línea
  * la primera línea se forma de un **tipo**, un **contexto** y una **descripción**
    ```
    tipo(contexto):descripción
    ```

  La línea no debiera tener más de 100 caracteres para que se lea bien en Github.

#### Tipo

El tipo nos ayuda a clasificar los commits. Los tipos que usamos son:

  * **feat**: Un nuevo feature
  * **fix**: La corrección de un bug
  * **docs**: Cambios en la documentación
  * **style**: Cambios que no afectan el significado del código (espacios, indentación, etc.)
  * **refactor**: Un cambio en el código que no agrega una funcionalidad ni corrige un bug
  * **perf** Cambios en el código que sólo mejoran la performance
  * **test**: Agrega, corrige o mejora tests
  * **chore**: Cambios al proceso de build y herramientas auxiliares

#### Contexto

  El contexto es una palabra que haga referencia al lugar del código donde afecta el commit.

#### Descripción

  * usamos el verbo imperativo en segunda persona(tú):  "cambia" no "cambio" tampoco "cambios"
  * sin mayúscula al principio
  * sin punto (.) al final


